/**
 * Adds a watcher to all of the supplied issues.
 * If there is partial success, the issues which we can modify will
 * be modified and the ones we cannot will be returned in an ArrayList.
 * @param issues the list of issues to update
 * @param currentUser the user to run the operation as
 * @param watcher the watcher to add to the issues
 * @return an ArrayList<Issue> containing the issues that could not be modified
 *
 * RC comments:
 * 1: Removed the expression return true on line 45 - otherwise all
 * issues would be added to successfulIssues.
 * 2: Please confirm the purpose of the if statement on lines 35-37.
 * If this if statement should remain in the method addWatcherToAll, add a
 * user watcherManager to the parameter list on lines 23-24.
 * At present, the method startWatching on line 36 takes the empty ArrayList<Issue>
 * successfulIssues as a parameter - is this correct or should it be changed to failedIssues?
 * The purpose of this if statement should be included in this comments section.
 * 3: Please confirm that the method getWatchingAllowed on line 43 returns a boolean value.
 * If it does not, change the method getWatchingAllowed to return a boolean value or
 * write a new method to replace it that does return a boolean value.
*/
public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User
currentUser, final User watcher, final User watcherManager) {		/*Added a user watcherManager*/
	ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
	ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
	for (Issue issue : issues) {
		if (canWatchIssue(issue, currentUser, watcher)) {
			successfulIssues.add (issue);
		}
		else {
			failedIssues.add (issue);
 		}
 	}
	if (successfulIssues.isEmpty()) {
		watcherManager.startWatching (currentUser, successfulIssues);
	}
	return failedIssues;
}

private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
	if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {
		return issue.getWatchingAllowed (watcher);
	}
	/*Removed return true - otherwise all issues would be added to successfulIssues*/
}